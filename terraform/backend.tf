provider "aws" {
  region = "ap-northeast-2"
}

terraform {
  backend "s3" {
    bucket = "build-landing-aws-jenkins-terraform-kkm"
    key = "static-terraform.tfstate"
    region = "ap-northeast-2"
  }
}
