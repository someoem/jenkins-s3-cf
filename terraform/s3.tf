resource "aws_s3_bucket" "www_site" {
  bucket = "www.${var.site_name}"

  website {
    index_document = "nyan.gif"
  }
}

resource "aws_s3_bucket_policy" "www_site_policy" {
  bucket = "${aws_s3_bucket.www_site.id}"
  policy = "${data.aws_iam_policy_document.bucket_policy.json}"
}